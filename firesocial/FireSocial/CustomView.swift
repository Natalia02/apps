//
//  CustomView.swift
//  FireSocial
//
//  Created by Ivan on 20/Apr/17.
//  
//

import UIKit

class CustomView: UIView {

    override open func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 1).cgColor
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 5, height: 10)
        layer.cornerRadius = 2.0
    }
}
